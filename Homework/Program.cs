﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            /*HumanBeing babySophie = new Child(new DateTime(2007,9,8),"Sophie");
            babySophie.Description();
            HumanBeing babyNick = (Child)babySophie.Clone();
            babyNick.Description();
            if (babyNick is Child) 
            {
                Child Nick = babyNick as Child;
                Nick.Name = "Nick";
                Nick.BirthDate = new DateTime(2018,5,16);
                Nick.Description();
            }
            HumanBeing mummy = new Woman(new DateTime(1973, 12, 12), "Inna","Her mother");
            mummy.Description();
            Woman dad = (Woman)mummy.Clone();
            dad.Name = "Joe";
            dad.Profession = "the best dad";
            dad.Description();*/
            HumanBeing babySophie = new Child(new DateTime(2007, 9, 8), "Sophie");
            babySophie.Description();
            HumanBeing babyNick = (Child)babySophie.MyClone();
            babyNick.Description();
            if (babyNick is Child)
            {
                Child Nick = babyNick as Child;
                Nick.Name = "Nick";
                Nick.BirthDate = new DateTime(2018, 5, 16);
                Nick.Description();
            }
            HumanBeing mummy = new Woman(new DateTime(1973, 12, 12), "Inna", "Her mother");
            mummy.Description();
            Woman dad = (Woman)mummy.MyClone();
            dad.Name = "Joe";
            dad.Profession = "the best dad";
            dad.Description();
            watch.Stop();
            Console.WriteLine($"Execution time in milliseconds: {watch.ElapsedMilliseconds}");
            Console.ReadKey();
        }
    }
}
