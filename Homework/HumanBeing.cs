﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    abstract class HumanBeing : ICloneable, IMyCloneable
    {
        public DateTime BirthDate { get; set; }
        public string Name { get; set; }
        public abstract object Clone();
        public abstract void Description();
        public abstract HumanBeing MyClone();
    }
}
