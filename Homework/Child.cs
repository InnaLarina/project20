﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Child : HumanBeing
    {

        public override object Clone()
        {
            return new Child(BirthDate, Name);
        }

        public override void Description()
        {
            DateTime now = DateTime.Today;
            int age = now.Year - BirthDate.Year;
            if (BirthDate > now.AddYears(-age)) age--;

            Console.WriteLine($"This is a pretty baby {Name}, she is {age} yes old!");
        }

        public override HumanBeing MyClone()
        {
            Child child;
            string Json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            child = Newtonsoft.Json.JsonConvert.DeserializeObject<Child>(Json);
            return child;
        }

        public Child(DateTime birthDate, string name)
        {
            this.BirthDate = birthDate;
            this.Name = name;
        }

    }
}
