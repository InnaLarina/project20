﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Woman : Child
    {
        public string Profession { get; set; }
        public Woman(DateTime birthdate, string name, string prof) : base(birthdate, name)
        {
            this.Profession = prof;
        }
        public override object Clone()
        {
            Child child = (Child)base.Clone();
            return new Woman(child.BirthDate, child.Name, Profession);
        }
        public override void Description()
        {
            Console.WriteLine($"This is a {Profession}, the name is {Name}.");
        }
        public override HumanBeing MyClone()
        {
            Child child = (Child)base.MyClone();
            return new Woman(child.BirthDate, child.Name, Profession);
        }
    }
}
